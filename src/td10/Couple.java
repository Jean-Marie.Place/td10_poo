package td10;

public class Couple<T, S> {
    private T un;
    private S deux;

    public Couple(T un, S deux) {
        this.un = un;
        this.deux = deux;
    }

    public boolean sameAs(Couple<T, S> other) {
        return this.un.equals(other.un) && this.deux.equals(other.deux);
    }

    public static <T, S> boolean sameCouple(Couple<T, S> first, Couple<T, S> second) {
        return first.un.equals(second.un) && first.deux.equals(second.deux);
    }

}