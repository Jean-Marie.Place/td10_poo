package td10;

import java.util.ArrayList;
import java.util.List;

public class Duo<T> {
    private T un;
    private T deux;

    public Duo(T un, T deux) {
        this.un = un;
        this.deux = deux;
    }

    public Duo(T meme) {
        this(meme, meme);
    }

    public void swap() {
        T temp;
        temp = this.un;
        this.un = this.deux;
        deux = temp;
    }

    public boolean identicalElements() {
        return un.equals(deux);
    }

    public boolean orderedEquals(Duo<T> other) {
        return this.un.equals(other.un) &&
            this.deux.equals(other.deux);
    }

    public boolean nonOrderedEquals(Duo<T> other) {
        return (this.un.equals(other.un) && this.deux.equals(other.deux))
        || (this.un.equals(other.deux) && this.deux.equals(other.un));
    }

    public static void main(String[] args) {
        Duo<String> duoStr = new Duo<String>("bruno","alice");
        System.out.println(duoStr);
        duoStr.swap();
        System.out.println(duoStr);
        Duo<Boolean> duoBool = new Duo<Boolean>(false,true);
        System.out.println(duoBool);
        duoBool.swap();
        System.out.println(duoBool);

        Duo<Integer> d1 = new Duo<>(5, 8);
        Duo<Integer> d2 = new Duo<>(8, 5);
        Duo<Integer> d3 = new Duo<>(5, 5);
        System.out.println(d1.nonOrderedEquals(d2));
        System.out.println(d1.nonOrderedEquals(d3));

    }

    private static<T>  void mixDuos(Duo<T> d1, Duo<T> d2) {
        T temp;
        temp = d1.un ;
        d1.un = d2.un;
        d2.un = temp;
    }

    static <T> List<Duo<T>> randomDuo(List<T> elements) {
        List<Duo<T>> liste = new ArrayList<>();
        
        while (elements.size() >= 2) {
            Duo<T> temp = new Duo<T>(elements.remove(0), elements.remove(1));
            liste.add(temp);
        }
        if (elements.size() > 0) {
            liste.add(new Duo<T>(elements.remove(0), null));
        }
        return liste;
    }
    
    @Override
    public String toString() {
        return "Duo [un=" + un + ", deux=" + deux + "]";
    }

}
