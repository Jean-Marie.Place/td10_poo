package td10;

import java.util.Scanner;

public class HumanPlayer implements IPlayer {
    private String name;
    private static Scanner sc = new Scanner(System.in);
    
    public HumanPlayer(String name) {
        this.name = name;
    }

    @Override
    public Couple<Mourre, Integer> play() {
        System.out.println(name + " ?");
        int mourre = sc.nextInt();
        int guess = sc.nextInt();
        return new Couple<Mourre, Integer>(Mourre.values()[mourre], guess);
    }
    
}
